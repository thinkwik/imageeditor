package com.thinkwik.thinkwikimageeditorlibrary

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity
import kotlinx.android.synthetic.main.custom_frag_dialog.view.*
import org.shagi.filepicker.ExtFile
import org.shagi.filepicker.FilePicker
import org.shagi.filepicker.FilePickerDialog
import org.shagi.filepicker.FilePickerFragment
import java.io.File

class EditImage(val activity: AppCompatActivity, var folderName: String, var reqCode: Int) {
    val EXTRA_OUTPUT = "extra_output"
    val IMAGE_IS_EDIT = "image_is_edit"
    var dialogFilter = DialogFilter()

    private fun createNewFile(name: String, ext: String): File? {
        try {
            val rootFile =
                File(Environment.getExternalStorageDirectory().toString() + File.separator + folderName + File.separator)
            rootFile.mkdirs()
            val sdImageMainDirectory = File(rootFile, "$name.$ext")
            return sdImageMainDirectory
        } catch (e: Exception) {
            Toast.makeText(
                activity, "Error occured. Please try again later.",
                Toast.LENGTH_SHORT
            ).show()
            return null
        }

    }

    fun pickImage(message: (msg: String) -> Unit, success: (file: File) -> Unit) {
        val pickerFragment = FilePickerFragment.getFragment(activity.supportFragmentManager, false)
        pickerFragment.use(CustomPicker(dialogFilter))
        pickerFragment.setOnLoadingListener(object : FilePicker.OnLoadingListener {
            override fun onLoadingStart(key: Long) {
                message("loading")
            }

            override fun onLoadingSuccess(key: Long, file: ExtFile) {
                success(file.file)

            }

            override fun onLoadingFailure(key: Long, throwable: Throwable) {
                message(throwable.message.toString())
            }

        })
        pickerFragment.show()
    }

    fun editImage(pickedFile: File) {
        val newFile =
            createNewFile(pickedFile.name + "_edited_" + System.currentTimeMillis(), pickedFile.extension)
        EditImageActivity.start(
            activity,
            pickedFile.absolutePath,
            newFile.toString(),
            reqCode,
            false
        );
    }

    @SuppressLint("ValidFragment")
    class CustomPicker(val dialogFilter: DialogFilter) : FilePickerDialog() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
            val v = inflater.inflate(R.layout.custom_frag_dialog, container, false)
            v.fdback.setBackgroundResource(dialogFilter.backgroundColor)
            v.ivCam.setImageResource(dialogFilter.cameraIcon)
            v.ivGal.setImageResource(dialogFilter.fileIcon)
            v.tvcam.setTextColor(activity!!.resources.getColor(dialogFilter.textColor))
            v.tvgal.setTextColor(activity!!.resources.getColor(dialogFilter.textColor))
            return v
        }

    }

    fun setStyle(styles: DialogFilter){
        dialogFilter = styles
    }

    data class DialogFilter(
        var backgroundColor: Int = R.color.backgroundColor,
        var textColor: Int = R.color.textColor,
        var cameraIcon: Int = R.drawable.file_picker_ic_camera,
        var fileIcon: Int = R.drawable.file_picker_ic_image
    )
}
