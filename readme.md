# ThinkwikImageEdit

###### !!! Some features may work unstable, make sure you have tested it. Thanks.

ThinkwikImageEdit is a simple image editing library developed at *Thinkwik India Online Services LLP*

The Library contains two major functionalities
1. Image Picker
2. Image Editor

Its a go-to solution for anyone  looking to add Image editing capabilities in their application

Picker will ask for camera and gallery permission on android version 6.0 or above.

# Install
To add this library to your project, you must add the JitPack repo to your root build.gradle file...

```groovy
allprojects {
 repositories {
    ...
    jcenter()
 }
}
```

[![](https://jitpack.io/v/org.bitbucket.thinkwik/imageeditor.svg)](https://jitpack.io/#org.bitbucket.thinkwik/imageeditor)
Then include this in your dependencies block

```groovy
implementation 'org.bitbucket.thinkwik:imageeditor:1.5.1'
```

# Usage

1. Add permissions and provider block to your's AndroidManifest.xml
```xml
    <uses-permission android:name="android.permission.CAMERA"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
```
2. For simple use instantiate EditImage and add few parameters to pass

```kotlin
    val saveFolder = "0MyPhotoEditor" //Folder in which you need to save Image
    val PHOTO_EDITOR_REQUEST = 999 //For handeling incoming edited Image from ImageEditor
    val editImage = EditImage(this@Activity, saveFolder, PHOTO_EDITOR_REQUEST)
```


# Available functions

## For Picking Images

```kotlin

        editImage.pickImage(
            message = { message ->
            // Information regarding image
        },
            success = { file ->
                // selected image is available in 'file'
            })

```
![screenshot](screenshot2.jpg) 


## For Editing Images

```kotlin

        editImage.editImage(file)

```
![screenshot](screenshot1.jpg)
 

## Receiving the output image

You can receive the new processed image path and it's edit status like this-
```kotlin
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PHOTO_EDITOR_REQUEST) { // same code you used while starting
            val isEdit = data!!.getBooleanExtra(editImage.IMAGE_IS_EDIT, false)
            if (isEdit) {
                val newFilePath = data!!.getStringExtra(editImage.EXTRA_OUTPUT)
                //You get the path of edited image
            }
        }
    }
```


## Styles

Add this to your app's `proguard-rules.pro` file -

```kotlin
    var styles = EditImage.DialogFilter()
    
    //Set Styles
    styles.backgroundColor = R.color.backgroundColor
            styles.textColor = R.color.textColor
            styles.cameraIcon = R.drawable.file_picker_ic_camera
            styles.fileIcon = R.drawable.file_picker_ic_image
    
    //Apply Styles
    editImage.setStyle(styles)
```

## Proguard Rules [Important!]

Add this to your app's `proguard-rules.pro` file -

```pro
-keepclasseswithmembers class * {
    native <methods>;
}
```

# License


    Copyright 2018 Thinkwik India Online Services LLP (https://www.thinkwik.com/)
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.