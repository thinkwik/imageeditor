package com.thinkwik.samplephotoeditor

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kotlinpermissions.KotlinPermissions
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import com.thinkwik.thinkwikimageeditorlibrary.EditImage
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {

    val saveFolder = "0MyPhotoEditor"
    val PHOTO_EDITOR_REQUEST = 999
    val editImage = EditImage(this@MainActivity, saveFolder, PHOTO_EDITOR_REQUEST)
    var styles = EditImage.DialogFilter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        styles.backgroundColor = R.color.backgroundColor
        styles.textColor = R.color.textColor
        styles.cameraIcon = R.drawable.file_picker_ic_camera
        styles.fileIcon = R.drawable.file_picker_ic_image

        editImage.setStyle(styles)

        openFileSelector.setOnClickListener {
            editImage.pickImage(message = { message ->
                textView.text = message
            },
                success = { file ->
                    Picasso.get()
                        .load(file)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(imageView)
                    editImage.editImage(file)
                })
        }

    }

    fun askPermissions() {
        KotlinPermissions.with(this) // where this is an FragmentActivity instance
            .permissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
            .onAccepted { permissions ->
                //List of accepted permissions

            }
            .onDenied { permissions ->
                //List of denied permissions
            }
            .onForeverDenied { permissions ->
                //List of forever denied permissions
            }
            .ask()

    }

    override fun onResume() {
        super.onResume()
        askPermissions()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PHOTO_EDITOR_REQUEST) { // same code you used while starting
            val isEdit = data!!.getBooleanExtra(editImage.IMAGE_IS_EDIT,false)
            if(isEdit){
                val newFilePath = data!!.getStringExtra(editImage.EXTRA_OUTPUT)
                Picasso.get()
                    .load(File(newFilePath))
                    .into(imageView)
            }
        }
    }


}
